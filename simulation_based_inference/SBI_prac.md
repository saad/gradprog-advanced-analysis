## Simulation Based Inference - Practical

In this practical we will:

- Create a toy SBI thingy from scratch
- Learn to use a proper SBI toolbox called ```sbi```...

Suppose you have a model of the form : ```param ---simulator--->data```

We are interested in making inference on the params given the data. In SBI, this is done in two steps:

- Step 1: Learn a nonlinear mapping :  ```data --> posterior``` where the posterior is parameterised in some way. This is done via forward simulations.
- Step 2: Given some actual data, use the mapping learned in step 1 to directly make inference

Before you start, make sure the following Python packages are installed (sorry about this):
* numpy
* matplotlib
* keras
* tensorflow
* scipy
* torch

Let's build our own dumb SBI 


```python
# Simple 1D example
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline

# Simulator does theta -> x
# here x = theta^3+noise
def simulator(theta, noise_std=.1):
    noise = noise_std * np.random.randn()
    return theta**3+noise

# Generate pairs of theta/data  using prior (doesn't work in practice!)
theta_prior_mean = 0.
theta_prior_std  = 1.

thetas = []
xs     = []
for n in range(1000):
    th = np.random.randn()*theta_prior_std + theta_prior_mean
    x  = simulator(th)
    thetas.append(th)
    xs.append(x)

```


```python
# Plot our samples
plt.plot(thetas,xs,'.')
plt.xlabel('theta')
plt.ylabel('x')
```

Now build a silly little neural net to generate params for approx posterior


```python
from keras.models import Model
from keras.layers import Dense, Input
import tensorflow as tf

L_0   = Input(shape=(1,))                   # Layer 0 takes in 1D data
L_1   = Dense(100,activation='tanh')(L_0)   # Layer 1 has 100 nodes and a tanh activation
L_2   = Dense(2,activation='linear')(L_1)   # Layer 2 outputs mean and logsig of the posterior
model = Model(L_0,L_2)


# This is the loss function:   mean[ -log q(theta;x) ]  where q is Gaussian
def my_loss(thetas,out_pred):
    mu        = out_pred[:,0][:,None]  # mean
    logsig    = out_pred[:,1][:,None]  # logsig
    sig       = tf.math.exp(logsig)
    loss = (thetas - mu)**2/sig**2/2 + logsig    
    return tf.reduce_mean(loss)

# some random optimizer
optim = tf.keras.optimizers.Adagrad(
    learning_rate=0.01, initial_accumulator_value=0.1, epsilon=1e-07,
    name='Adagrad')

# compile the model (whatever that means in Keras world)
model.compile(loss=my_loss,optimizer=optim)

# train the model
history = model.fit(xs, thetas, epochs=100, batch_size=16,workers=4)



```

### What have we just done?

We have trained a neural net to take in some *simulated* data and output the mean and the log(std) of a Gaussian distribution. 

Given that the data were simulated from the prior on theta, and given the form of our cost function, the optimal mean and log(std) are those of the posterior distribution (assuming it is a Gaussian). 

For more "expressive" posterior distibutions, one can use mixtures of Gaussians for example.

Let's now visualise the fit to the training data by plotting the data on the x-axis and the parameters on the y-axis (compare this to the previous plot). We also plot the mean posterior distribution.


```python
# Visualise the fit to the training data

mus    = model.predict(xs)[:,0]
plt.plot(xs,thetas,'.')
plt.plot(xs,mus,'.')
plt.xlabel('data')

```

Now let us have a look at the posterior for a new data point that is not part of the training set. For this we need to 'observe' new data.
We will just generate those using our simulator


```python

true_theta  = np.random.randn()*theta_prior_std+theta_prior_mean  # generate a parameter theta (here using the prior but we don't have to)
observation = simulator(true_theta) # simulate the data
mu,logsig   = model.predict([observation])[0] # use our neural net to predict the parameters of the posterior

# Plotting the prior, posterior, and actual theta value
from scipy.stats import norm
x_axis = np.linspace(-2, 2,1000)

plt.figure()
plt.plot(x_axis, norm.pdf(x_axis,mu,np.exp(logsig)),label='approx-posterior')
plt.plot(x_axis, norm.pdf(x_axis,theta_prior_mean,theta_prior_std),c='k',alpha=.3,label='prior')
plt.axvline(x=true_theta,c='r',label='actual')
plt.legend()
plt.show()


```

Not bad!  ~~Bad~~ 
(delete as appropriate. It doesn't work for every theta in my experience)

Now let's use an SBI toolbox to do the same thing


```python
import torch
import sbi.utils as utils
from sbi.inference.base import infer

# Same simulator as before
# (note: we did not try to infer the internal state parameter: noise_std)
def simulator(theta, noise_std=.1):
    noise = noise_std * np.random.randn()
    return theta**3 + noise

# Priors
theta_prior_mean = 0.
theta_prior_std  = 1.
prior = torch.distributions.Normal(loc=torch.tensor([theta_prior_mean]),
                                   scale=torch.tensor([theta_prior_std]))

# Below is SNPE-C method if I am not mistaken
# Feel free to check the documentation and try out different methods

# Simulator is user-defined function
# Prior must be a ...
posterior   = infer(simulator, prior, method='SNPE', num_simulations=1000,num_workers=8)



```

### What have we done?  Same thing as before! We have trained a neural net to map from data to posterior

Let us now see what happens when we have a new observation


```python

# Generate an observation
true_theta  = np.random.randn()*theta_prior_std+theta_prior_mean
observation = simulator(true_theta)

# Sample from the *now amortised* posterior
samples  = np.asarray(posterior.sample((1000,), x=observation))
mu       = np.mean(samples)
sig      = np.std((samples))

# Plot
from scipy.stats import norm
x_axis = np.linspace(-2, 2,1000)
plt.plot    (x_axis, norm.pdf(x_axis,mu,sig),label='approx-posterior')
plt.plot    (x_axis, norm.pdf(x_axis,theta_prior_mean,theta_prior_std),c='k',alpha=.3,label='prior')
plt.axvline (x=true_theta,c='r',label='actual')
plt.legend()
plt.show()

```

Soup-Herb!

Now let us use this SBI toolbox on a more interesting simulator: the ball and stick model


```python
import torch
import numpy as np

import sbi.utils as utils
from sbi.inference.base import infer

# read in bvals and bvecs
bvals = torch.tensor(np.loadtxt('./data/bvals'),dtype=float)/1000.0
bvecs = torch.tensor(np.loadtxt('./data/bvecs'),dtype=float)

# forward model is ball and stick
def forward(p):
    s0,d,th,ph,f = p
    # spherical->cartesian
    x   = torch.tensor([torch.sin(th)*torch.cos(ph),torch.sin(th)*torch.sin(ph),torch.cos(th)])
    # sqr dot prod of fibre orientation and bvec
    ang = torch.sum((x[:,None]*bvecs)**2,axis=0)
    # return predicted signal
    return s0*((1-f)*torch.exp(-bvals*d)+f*(torch.exp(-bvals*d*ang)))

# simulator generates noisy data
def simulator(parameter_set,noise_std = .01):    
    noise     = torch.randn(bvals.shape) * noise_std
    return forward(parameter_set) + noise

# Priors (uniform in this case)
#                                             s0  d   th     ph        f
prior = utils.BoxUniform(low  = torch.tensor([0., 0., -10.,  -10.,   0.]), 
                         high = torch.tensor([2., 3., 10.,   10.,    1.]))



```


```python
# Before we see any data, train neural net
posterior = infer(simulator, prior, method='SNPE', num_simulations=1000,num_workers=8)

```


```python
# Generate a new observation
true_p      = torch.tensor([0.7000, 0.3000, 0.7854, 1.0472, 0.3000])
observation = simulator(true_p)
plt.plot(observation)
```


```python
# Sample from posterior
samples     = posterior.sample((10000,), x=observation)
```

### Now lett us plot the posterior distribution

Because we have multiple paraameters, it is useful to not only plot the marginal for each param, butt also the joint posteriors for pairs of params. Thee SBI toolbox has a useful utility for this.


```python
from sbi import utils as utils

fig, axes = utils.pairplot(samples,
                           fig_size=(15,15),
                           points=true_p,labels=['s0','d','th','ph','f'],
                           points_offdiag={'markersize': 16},
                           points_colors='r');
```

You should be able to see the bimodal distribution for phi (as the model doesn't care about the sign of phi). You should also note how incredibly fast it is to generate samples compared to MCMC. 

Also the results here my appear disappointing but the prior for theta/phi is very bad, as it is not uniform on the sphere. Nevermind...

*The End.*




```python

```


```python

```


```python

```


```python

```
