# This is the ICA (Independent Component Analysis) practicals.

The practical is in Matlab. You will need to start matlab and you can either copy-paste the code from the ```ica_prac.m``` file or open the file directly in the Matlab editor.
