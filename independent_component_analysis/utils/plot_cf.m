function plot_cf(X,cf)

angles = linspace(0,2*pi,1000);   % angles to calculate cost function along
C      = zeros(size(angles));     % Cost
for i=1:length(angles)
    % get vector w along angle 
    [w_x,w_y] = pol2cart(angles(i),1);
    w = [w_x;w_y];
    % calculate cf along w
    C(i) = mean(cf(w'*X));
end


hold on
plot(X(1,:),X(2,:),'bo')

[xx,yy] = pol2cart(angles,C);
plot(xx,yy,'linewidth',2,'color','k','linestyle','--')
axis equal
grid on


