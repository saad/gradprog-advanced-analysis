# This is the MCMC (Markov chain Monte Carlo) practical

The practical is in Python. There is a Notebook version (.ipynb file) and a Markdown version (.md).

To use the Notebook version, you need to start a jupyter notebook and open the file within the notebook.

You may also want to just view the Markdown and copy-paste the code into your favourite Python shell.


