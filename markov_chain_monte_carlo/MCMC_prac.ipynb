{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "035e26fa",
   "metadata": {},
   "source": [
    "## Markov chain Monte Carlo (MCMC) - Practical"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "462c993d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# import some useful libaries\n",
    "import numpy as np               # numeric library\n",
    "import matplotlib.pyplot as plt  # main plotting library\n",
    "import seaborn as sns            # additional plotting library"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97d19e5a",
   "metadata": {},
   "source": [
    "### Bayesian model inversion\n",
    "\n",
    "In this practical we will do Bayesian model inversion using MCMC for some simple models.\n",
    "\n",
    "\n",
    "Bayesian model inversion assumes we have some forward model mapping parameters to data.\n",
    "We then want to invert this to map some observed data to the parameters.\n",
    "\n",
    "This inversion is done through Bayes equation (derived in the lecture):\n",
    "$$P(\\vec{\\theta}|\\vec{x}) = P(\\vec{x}|\\vec{\\theta}) P(\\vec{\\theta}) / P(\\vec{x}),$$\n",
    "where\n",
    "- $P(\\vec{\\theta}|\\vec{x})$ is the probability distribution of the parameters $\\vec{\\theta}$ conditional on the data $\\vec{x}$. This is called the posterior and is what we want to derive in Bayesian model inversion.\n",
    "- $P(\\vec{x}|\\vec{\\theta})$ is the probability distribution of the data $\\vec{x}$ conditional on the parameters $\\vec{\\theta}$. This is called the likelihood. It is defined by our forward model.\n",
    "- $P(\\vec{\\theta})$ is the probability distribution of the parameters independent of the data. This is called the prior and should reflect our prior belief of what the parameters were before seeing the data. In order to not bias the analysis, one often uses uninformative (i.e., very broad) prior distribution. A common choice is $P(\\vec{\\theta}) = 1$ or $P(\\vec{\\theta}) = 1/\\vec{\\theta}$.\n",
    "- $P(\\vec{x})$ is the probability of getting the data integrated over all possible parameters. It is called the marginal likelihood or evidence and reflects how likely one is to get the data given the forward model.\n",
    "\n",
    "As we will see below, the likelihood and prior are usually given by the user based on their forward model for the data. \n",
    "The marginal likelihood can then be computed as:\n",
    "$$P(\\vec{x}) = \\int P(\\vec{x}, \\vec{\\theta}) d\\vec{\\theta} = \\int P(\\vec{x}|\\vec{\\theta}) P(\\vec{\\theta}) d\\vec{\\theta}$$\n",
    "So, we can compute the marginal likelihood as the integral of the likelihood multiplied by the prior over all possible parameters. \n",
    "Once we have computed this marginal likelihood (which is just a scalar given the observed data) we can divide the likelihood and prior by this number to get the posterior distribution.\n",
    "\n",
    "Unfortunately, this requires integrating over all possible parameters, which will be impractical for all but the most simple cases.\n",
    "So, we need an algorithm that allows us to estimate properties of the posterior without having to compute the marginal likelihood, namely MCMC\n",
    "\n",
    "### Markov chain Monte Carlo (MCMC)\n",
    "#### Metropolis-Hastings MCMC algorithm\n",
    "In MCMC we generate samples from the posterior distribution without explicitly having to know what the posterior distribution looks like.\n",
    "We do this by following a random walk through parameter space using the following algorithm:\n",
    "- given the location of the random walk in parameter space $\\vec{\\theta}_n$ in step $n$, generate some candidate paraemters from a proposal distribution $\\vec{\\theta}_c \\sim P_{\\rm prop}(\\vec{\\theta}_c|\\vec{\\theta_n})$. A common choice for proposal distribution is a Gaussian centered on $\\vec{\\theta_n}$.\n",
    "- Draw a random number between 0 and 1. If this number is larger than the posterior ratio ($P(\\vec{\\theta}_c|\\vec{x})/P(\\vec{\\theta}_n|\\vec{x})$) accept the new point. This in effect gives an acceptance probability of:\n",
    "  $$P_{\\rm accept} = {\\rm min}(P(\\vec{\\theta}_c|\\vec{x})/P(\\vec{\\theta}_n|\\vec{x}),1)$$\n",
    "  - If accepted the next point is the candidate point: $\\vec{\\theta}_{n+1} = \\vec{\\theta}_{c}$\n",
    "  - If rejected the next point is the original point: $\\vec{\\theta}_{n+1} = \\vec{\\theta}_{n}$\n",
    "\n",
    "Crucially, we can compute the posterior ratio ($P(\\vec{\\theta}_c|\\vec{x})/P(\\vec{\\theta}_n|\\vec{x})$) without computing the marginal likelihood.\n",
    "This is because when we enter Bayes equation, the marginal likelihood drops out.\n",
    "Instead the posterior ratio is given by the multiplication of the ratio of the likelihoods and the priors:\n",
    "$$\\frac{P(\\vec{\\theta}_c|\\vec{x})}{P(\\vec{\\theta}_n|\\vec{x})} = \\frac{P(\\vec{x}|\\vec{\\theta}_c)}{P(\\vec{x}|\\vec{\\theta}_n)} \\frac{P(\\vec{\\theta}_c)}{P(\\vec{\\theta}_n)}$$\n",
    "\n",
    "Let's start with a simple example, where are data is drawn from a Gaussian distribution with unknown mean, but known standard devation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82429bb5",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(1)  # set seed to get consistent results\n",
    "\n",
    "true_mu = 1.3  # unknown\n",
    "sigma = 1  # known\n",
    "x = true_mu + np.random.randn(5) * sigma  # draw 5 samples"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6398a218",
   "metadata": {},
   "source": [
    "Our likelihood function in this case is:\n",
    "$$P(\\vec{x} | \\mu) = N(\\mu, \\sigma^2=1)$$\n",
    "We will assume an uninformative prior of $P(\\mu) = 1$.\n",
    "So the posterior ratio is given by:\n",
    "$$\\frac{P(\\mu_c|\\vec{x})}{P(\\mu_n|\\vec{x})} = \\frac{P(\\vec{x}|\\mu_c)}{P(\\vec{x}|\\mu_n)} \\frac{P(\\mu_c)}{P(\\mu_n)} = \\frac{N(\\mu_c, \\sigma^2=1)}{N(\\mu_n, \\sigma^2=1)}$$\n",
    "In code this looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d377bce9",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(2)  # set seed to get consistent results\n",
    "\n",
    "all_samples = []\n",
    "current_mu = 0.  # we have to start our random walk somewhere...\n",
    "for _ in range(10000):  # let's run the MCMC for 10000 steps\n",
    "    # Draw from Gaussian proposal function\n",
    "    candidate = np.random.randn() + current_mu\n",
    "\n",
    "    # Compute posterior ratio\n",
    "    ratio = np.prod(np.exp(-(x - candidate) ** 2 / 2 * sigma ** 2)) / np.prod(np.exp(-(x - current_mu) ** 2 / 2 * sigma ** 2))\n",
    "    if np.random.rand() < ratio:  # accept if random number < P_accept\n",
    "        current_mu = candidate\n",
    "    else:                         # otherwise reject\n",
    "        current_mu = current_mu\n",
    "\n",
    "    # always add new point to the chain, even if candidate got rejected!\n",
    "    all_samples.append(current_mu)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2222af97",
   "metadata": {},
   "source": [
    "Let's look at the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad5c777b",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(all_samples, bins=51, label='MCMC samples', density=True)\n",
    "plt.axvline(true_mu, label='true mean', color='black')\n",
    "\n",
    "sample_mean = np.mean(x)\n",
    "sample_mean_std = sigma / np.sqrt(len(x))\n",
    "\n",
    "xval = np.linspace(-4, 4, 101) * sample_mean_std + sample_mean\n",
    "plt.plot(xval, 1/np.sqrt(2 * np.pi * sample_mean_std ** 2) * np.exp(-(xval - sample_mean) ** 2 / (2 * sample_mean_std ** 2)), label='analytical solution')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97aeb7e2",
   "metadata": {},
   "source": [
    "Some caveats:\n",
    "- In practice we will nearly always work with log-probability rather than probality functions. \n",
    "  So rather than computing the posterior ratio as `ratio = np.prod(np.exp(-(x - candidate) ** 2 / 2)) / np.prod(np.exp(-(x - current_mu) ** 2 / 2))`\n",
    "  we would compute the posterior ratio as `ratio = np.exp(0.5 * (np.sum(-(x - candidate) ** 2)) - np.sum(-(x - current_mu) ** 2)))`.\n",
    "  This has the advantage that the product in the first case can become extremely small, which can lead to numerical instabilities in the calculations. We will see this in action below.\n",
    "- The acceptance rate is given by the posterior distribution only for a *symmetric* proposal function. \n",
    "  For asymmetric proposal functions (i.e., $P_{\\rm prop}(\\vec{\\theta}_c|\\vec{\\theta_n}) \\ne P_{\\rm prop}(\\vec{\\theta}_n|\\vec{\\theta_c}))$), the acceptance probability will also depend on the ratio of the acceptance probabilities (see https://en.wikipedia.org/wiki/Metropolis%E2%80%93Hastings_algorithm). Here we will only investigate symmetric proposal distributions.\n",
    "\n",
    "#### Multi-parameter model example\n",
    "First we define a generic MCMC algorithm. We assume the user provides some function $f$ which gives the sum \n",
    "of the log-likelihood and log-prior given some set of parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d155814",
   "metadata": {},
   "outputs": [],
   "source": [
    "def metropolis_hastings(f, x0, nsteps=10000, step_size=1.):\n",
    "    \"\"\"MCMC using Metropolis-Hastings algorithm\n",
    "\n",
    "    Args:\n",
    "        f: function mapping vector of parameters to sum of log-likelihood and log-prior\n",
    "        x0: starting set of parameters\n",
    "        nsteps: how many steps to run the MCMC for\n",
    "        step_size: size of the Gaussian proposal function\n",
    "\n",
    "    Returns:\n",
    "        2D array (nsteps x nparams) with the Markov chain through parameter space\n",
    "    \"\"\"\n",
    "    params = []\n",
    "\n",
    "    current = x0\n",
    "    current_f = f(x0)\n",
    "    for _ in range(nsteps):\n",
    "        candidate = np.random.randn(len(current)) * step_size + current\n",
    "        candidate_f = f(candidate)\n",
    "        ratio = np.exp(candidate_f - current_f)\n",
    "        if np.random.rand() < ratio:\n",
    "            current = candidate\n",
    "            current_f = candidate_f\n",
    "        params.append(current)\n",
    "    return np.array(params)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ffec2f5",
   "metadata": {},
   "source": [
    "This is the full MCMC algorithm. Make sure you understand what is going on!\n",
    "\n",
    "Let's see this in action for another simple Gaussian example.\n",
    "This time we assume the variance is also unknown:\n",
    "$$P(\\vec{x} | \\mu, \\sigma^2) = N(\\mu, \\sigma^2)$$\n",
    "We will assume uninformative Jeffrey's priors of $P(\\mu) = 1$ and $P(\\sigma^2) = 1/\\sigma^2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e8b501a",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(1)  # set seed to get consistent results\n",
    "\n",
    "true_mu = 1.3  # unknown\n",
    "true_std = 1  # unknown\n",
    "x = true_mu + np.random.randn(5) * true_std  # draw 5 samples\n",
    "print(x)\n",
    "\n",
    "def log_likelihood_prior(params):\n",
    "    mu, variance = params\n",
    "    if variance < 0:\n",
    "        return -np.inf  # will cause any candidated with negative variance to be rejected. This allows one to set constraints on parameters in MCMC\n",
    "    log_likelihood = 0.5 * np.sum(-(x - mu) ** 2 / variance) - 0.5 * len(x) * np.log(variance)  # ignoring constant term\n",
    "    log_prior = -np.log(variance)\n",
    "    return log_likelihood + log_prior\n",
    "\n",
    "samples = metropolis_hastings(log_likelihood_prior, [0, 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4dc5791",
   "metadata": {},
   "source": [
    "Seaborn allows us to plot the samples from the posterior distribution in 2D (compare with lecture)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53d090e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "as_dict = {\n",
    "    'mean': samples[:, 0],\n",
    "    'variance': samples[:, 1],\n",
    "}\n",
    "jp = sns.jointplot(x='mean', y='variance', data=as_dict)\n",
    "\n",
    "# plot sample mean and variance as black lines\n",
    "jp.ax_joint.axvline(np.mean(x), color='black')\n",
    "jp.ax_joint.axhline(np.var(x, ddof=1), color='black')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37f33f8c",
   "metadata": {},
   "source": [
    "We can characterise the posterior distribution by looking at summary measures of our samples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bbea6a3d",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'P(mu > 0) = {np.sum(samples[:, 0] > 0) / samples.shape[0]}')\n",
    "print(f'Expectation value of mu (E(mu) = {np.mean(samples[:, 0])}) matches the sample mean ({np.mean(x)})')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6482d5e",
   "metadata": {},
   "source": [
    "Here we see that we can estimate the probability of the mean being bigger than zero by the fraction of samples bigger than zero. Similarly, we can estimate the mean of the distribution by computing the mean of the samples.\n",
    "Note that the precision of these estimates is limited by the number of *independent* samples.\n",
    "\n",
    "### Automatically adjusting MCMC step size\n",
    "The efficiency of the MCMC algorithm will depend greatly on how closely the proposal function matches the target posterior function.\n",
    "The closer they match, the more efficient the algorithm.\n",
    "This can be best visualised using the trace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12b01553",
   "metadata": {},
   "outputs": [],
   "source": [
    "# use the same model/data as previous section\n",
    "np.random.seed(1)  # set seed to get consistent results\n",
    "\n",
    "fig, axes = plt.subplots(3, 1, figsize=(4, 10), sharex=True)\n",
    "for ax, step_size, label in zip(\n",
    "    axes, (0.05, 1, 10), ('too small', 'decent', 'too large')\n",
    "):\n",
    "    # run MCMC three times with three different step sizes\n",
    "    samples = metropolis_hastings(log_likelihood_prior, [0, 1], step_size=step_size)\n",
    "    ax.plot(samples[:, 0])  # the trace is simply a line plot showing the evolution of some parameters during the MCMC\n",
    "    ax.set_ylabel(r'$\\mu$')\n",
    "    ax.set_title(f'step size is {label}')\n",
    "axes[-1].set_xlabel('step time (t)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd86bb27",
   "metadata": {},
   "source": [
    "We want subsequent samples to look independent from each other (i.e., middle panel) rather than varying very slowly (indicative of too small step size; top panel) or getting stuck (indicative of too large step size; bottom panel). Note that irrespective of step size, the distribution of MCMC will converge to the posterior distribution. With the wrong step size it just might take much, *much* longer...\n",
    "\n",
    "A nice way to ensure the step size is correct (and deal with correlated parameters) is to adjust the step size automatically.\n",
    "To get correlated parameters we will fit a straight line to data that has not been demeaned. So the model is:\n",
    "$$y = a x + b + \\epsilon$$\n",
    "or in terms of probabilities\n",
    "- Likelihood: $P(y| a, x, b, \\sigma^2) = N(a x + b, \\sigma^2)$\n",
    "- Prior: $P(a, b, \\sigma^2) = 1/\\sigma^2$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1a2a661",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(1)\n",
    "x = np.random.randn(10) + 6\n",
    "true_a = 1\n",
    "true_b = 0\n",
    "true_variance = 10\n",
    "y = true_a * x + true_b + np.random.randn(10) * np.sqrt(true_variance)\n",
    "\n",
    "def logp(params):\n",
    "    a, b, var = params\n",
    "    if var < 0:\n",
    "        return -np.inf\n",
    "    offset = y - (a * x + b)\n",
    "    log_likelihood = -0.5 * np.sum(offset ** 2 / var) - 0.5 * len(x) * np.log(var)\n",
    "    log_prior = -np.log(var)\n",
    "    return log_likelihood + log_prior\n",
    "\n",
    "plt.plot(x, y, '.')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "\n",
    "# extend x- and y-axes to include origin\n",
    "plt.xlim(0, plt.xlim()[1])\n",
    "plt.ylim(0, plt.ylim()[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d07ec491",
   "metadata": {},
   "source": [
    "First we update the Metropolis-Hastings function to accept\n",
    "a variance matrix. \n",
    "The proposal function will be drawn from a Gaussian with this variance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5d36c76",
   "metadata": {},
   "outputs": [],
   "source": [
    "def metropolis_hastings_cov(f, x0, variance_matrix, nsteps=10000):\n",
    "    \"\"\"MCMC using Metropolis-Hastings algorithm\n",
    "\n",
    "    Args:\n",
    "        f: function mapping vector of parameters to sum of log-likelihood and log-prior\n",
    "        x0: starting set of parameters\n",
    "        variance_matrix: variance to use for Gaussian proposal function\n",
    "        nsteps: how many steps to run the MCMC for\n",
    "\n",
    "    Returns:\n",
    "        2D array (nsteps x nparams) with the Markov chain through parameter space\n",
    "    \"\"\"\n",
    "    params = []\n",
    "\n",
    "    # take square root of matrix for sampling\n",
    "    sample_mat = np.linalg.cholesky(variance_matrix)\n",
    "\n",
    "    current = x0\n",
    "    current_f = f(x0)\n",
    "    for _ in range(nsteps):\n",
    "        candidate = sample_mat @ np.random.randn(len(current)) + current\n",
    "        candidate_f = f(candidate)\n",
    "        ratio = np.exp(candidate_f - current_f)\n",
    "        if np.random.rand() < ratio:\n",
    "            current = candidate\n",
    "            current_f = candidate_f\n",
    "        params.append(current)\n",
    "    return np.array(params)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e25cbf7f",
   "metadata": {},
   "source": [
    "Then we define a new function, which estimates the best variance for the Gaussian proposal function in an iterative manner:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1cf1d682",
   "metadata": {},
   "outputs": [],
   "source": [
    "def iterative_metropolis_hastings(f, x0, nsteps=10000, niter=3):\n",
    "    \"\"\"MCMC using Metropolis-Hastings algorithm\n",
    "\n",
    "    Proposal variance is determined iteratively\n",
    "\n",
    "    Args:\n",
    "        f: function mapping vector of parameters to sum of log-likelihood and log-prior\n",
    "        x0: starting set of parameters\n",
    "        nsteps: how many steps to run the MCMC for\n",
    "        niter: number of iterations to estimate the covariance before running the final MCMC\n",
    "\n",
    "    Returns:\n",
    "        2D array (nsteps x nparams) with the Markov chain through parameter space\n",
    "    \"\"\"\n",
    "    # start with small, isotropic step size\n",
    "    current_variance_mat = np.eye(len(x0)) * 1e-2\n",
    "\n",
    "    for _ in range(niter):\n",
    "        # For each iteration we start by running a short (i.e., with reduced number of steps) MCMC\n",
    "        samples = metropolis_hastings_cov(f, x0, current_variance_mat, nsteps=nsteps//10)\n",
    "\n",
    "        # set the new variance matrix to the covariance of the samples of this MCMC (divided by the number of parameters)\n",
    "        current_variance_mat = np.cov(samples.T) / len(x0)\n",
    "        x0 = samples[-1]  # also update starting position\n",
    "\n",
    "    # Finally, run the full MCMC with the covariance of the proposal functions set by the last iteration\n",
    "    return metropolis_hastings_cov(f, x0, current_variance_mat, nsteps=nsteps)  # run final MCMC with all steps"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe8fe1aa",
   "metadata": {},
   "source": [
    "Without any iterations we can see from the trace that the initial step size is clearly too small:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67fdf99e",
   "metadata": {},
   "outputs": [],
   "source": [
    "samples_no_iteration = iterative_metropolis_hastings(logp, [0, 0, 1.], niter=0)\n",
    "plt.plot(samples_no_iteration[:, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d8f27f6",
   "metadata": {},
   "source": [
    "However, after several iterations of updating the proposal function covariance matrix, we get a nice trace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbe566c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "iter_samples = iterative_metropolis_hastings(logp, [0, 0, 1.], niter=3)\n",
    "plt.plot(iter_samples[:, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a81b2762",
   "metadata": {},
   "source": [
    "The resulting distribution of a and b looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59aafc93",
   "metadata": {},
   "outputs": [],
   "source": [
    "as_dict = {\n",
    "    'a': iter_samples[:, 0],\n",
    "    'b': iter_samples[:, 1],\n",
    "    'variance': iter_samples[:, 2],\n",
    "}\n",
    "sns.jointplot(x='a', y='b', data=as_dict)\n",
    "\n",
    "# add true value as red star\n",
    "jp.ax_joint.scatter(true_a, true_b, color='r', marker='*')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f70eef0",
   "metadata": {},
   "source": [
    "Ideas for further exploration:\n",
    "- Figure out how many iterations are needed to get an efficient sampling of the posterior\n",
    "- Define your own forward model (i.e., likelihood & priors). Note that to run MCMC, you would only have to write a function that computes the sum of the log_likelihood and log_prior given some parameters. You can then use the MCMC algorithms above to sample the posterior.\n",
    "- Test MCMC for a model with (many) more parameters."
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 5
}
