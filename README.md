# Graduate Course : Advanced Analysis series

This repository contains a compilation of practicals for the Advanced Analysis part of the WIN@FMRIB Graduate Programme.

Each folder contains practical exercises to accompany a talk. These are typically in Matlab or Python, and typically take you through some code that replicates the figures in the talk. They can also be used independently of the talks.


## Instructions 

The repo can be cloned using:

```
git clone https://git.fmrib.ox.ac.uk/saad/gradprog-advanced-analysis.git
```

If you want to run (most of) the Jupyter notebooks you can do so using:

```
cd gradprog-advanced-analysis
fslpython -m notebook
```

A page should open in your web browser - to access the practicals, navigate
into one of the folders and click on the `.ipynb` file you are interested in.

**Note**: The Transformers practical has it's own instructions (in the Transformers folder) as it requires PyTorch which is not included by default with fslpython. The CNNs practical is Google colab based, and just requires following a link in the CNNs folder.

Some practicals are matlab based, in which case you will need to start matlab and navigate to the appropriate .m file in the sub-directories.

## Contributors

Jesper Andersson, Saad Jbabdi, Soojin Lee, Andrew Quinn, Michiel Cottaar, Rick Lange, Ziyu Li

