# Markov Models

Two types of Markov Models are covered here. They both follow this graphical structure:


![Markov Models](markov.png)


- Hidden Markov Models (HMMs) : when the hidden states take on discrete values
- State space models (aka Kalman Filter) : when the hidden states take on continuous values