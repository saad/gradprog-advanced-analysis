# Kalman Filter practical

This is a very short Matlab practical that implements a 1D, linear Kalman Filter. 

Please refer to the presentation slides for the derivation of the update equations.
    