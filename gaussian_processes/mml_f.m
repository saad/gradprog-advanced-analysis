function logp = mml_f(p,y,D2)
% log-p for marginal likelihood of GP with squared exponential
% FORMAT logp = mml(p,y,D2)
%
% p:  Hyper-parameters:
%     p(1): standard deviation of signal (sigma_f)
%     p(2): lenght-scale of signal (ell)
%     p(3): standard deviation of noise (sigma_n)
% y:  Data
% D2: Squared-distance matrix
%

K = p(1)^2*exp(-0.5*D2/p(2)^2) + p(3)^2*eye(size(D2));
L = chol(K,'lower');
data_term = 0.5 * y'*(L'\(L\y));          % 0.5 * y'*inv(K)*y
complexity = 0.5 * 2 * sum(log(diag(L))); % 0.5*log(det(K)), Extra 2 because K=L^2
constant = 0.5 * length(y) * log(2*pi);   % Just for pedagogical reasons

logp = data_term + complexity + constant;

return