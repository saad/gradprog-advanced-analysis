# Deep Learning Practicals

This folder contains introductions to topics in deep learning. Currently it covers:

- CNNs : Convolutional Neural Networks
- GANs : Generative Adversarial Networks
- VAEs : Variational Autoencoders
- RNNs : Recurrent Neural Networks
- Transformers : Visual Transformer Networks

Pick a folder and run the jupter notebooks within.

Note: The Transformers notebook requires setting up a conda environment before running.