# Variational AutoEncoders

The notebooks cover:

- `Part 1`: Simple implementation of PCA and Autoencoders (not variationals) in a simple 1D-to-1D example. you will learn the similarities and differences between PCA (a linear auto-encoder) and AEs (which can be nonlinear) and you will be able to easily understand their behaviour because of the siimplicity of the input and output.
- `Part 2`: Application of Auto-encoders to encoding images (the MNIST dataset), where you will learn how to use AEs for multi-dimensional input and why vanilla AEs do not give good low-dimensional embeddings.
- `Part 3`: Introducing the variational part of VAEs and applying to the MNIST dataset.