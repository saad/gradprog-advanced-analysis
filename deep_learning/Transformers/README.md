# Transformers/Attention practical

This practical is a minor adaptation of one developed by [Dr Nicola Dinsdale](https://www.pmb.ox.ac.uk/person/dr-nicola-dinsdale), and we would like to thank her for allowing us to use it here. The original practical was based, with permission, on [this](https://medium.com/mlearning-ai/vision-transformers-from-scratch-pytorch-a-step-by-step-guide-96c3313c2e0c) blog post.

Note that this is a relatively advanced practical compared to the other DL practicals.

## Running the Practical

To run this practical, you must first create a conda environment with the necessary libraries installed. Please run the following command from within the `transformers` folder:

```
$FSLDIR/bin/conda create --prefix ./agc_transformers_env -c conda-forge python=3.8 notebook numpy matplotlib pytorch scikit-learn
```

When that has completed, run:

```
source $FSLDIR/bin/activate ./agc_transformers_env
jupyter notebook
```

This will start a Jupyter Notebook session in your browser and you should then be able to open the `transformers.ipynb` file and run the practical.

### NOTE: Older FSL Installations
If the above does not work for you, you may be running an older version of FSL. Please try:


```
$FSLDIR/fslpython/bin/conda create --prefix ./agc_transformers_env -c conda-forge python=3.8 notebook numpy matplotlib pytorch scikit-learn
```

When that has completed, run:

```
source $FSLDIR/fslpython/bin/activate ./agc_transformers_env
jupyter notebook
```