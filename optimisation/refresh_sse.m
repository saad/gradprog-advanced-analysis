function refresh_sse(beta1,beta2,sse,lcm,b)

clf;
surfc(beta1,beta2,sse,'EdgeColor',[.5 .5 .5]);
xlabel('b1');
ylabel('b2');
view(-70,52);
axis([3 6 -0.05 0.2 0 4000]);

colormap(lcm);

hold on
plot3(b(1),b(2),b(3),'ro','MarkerSize',12,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

return