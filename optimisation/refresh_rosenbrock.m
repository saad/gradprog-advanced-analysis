function refresh_rosenbrock(x,y,lcm,pp)

rosen = (1-x).^2 + 100*(y-x.^2).^2;    % Function values on grid
clf;
surfc(x,y,rosen,'EdgeColor',[.5 .5 .5]);
xlabel('x');
ylabel('y');

colormap(lcm);
axis([-1.5 2 -1 3 -200 2500]);
view(-28,30);

hold on;
xm=1; ym=1; fm=(1-xm).^2 + 100*(ym-xm.^2).^2;
plot3(xm,ym,fm,'ro','MarkerSize',12,'MarkerFaceColor','r','MarkerEdgeColor','k','LineWidth',3);

for i=1:size(pp,2)
   fp=(1-pp(1,i)).^2 + 100*(pp(2,i)-pp(1,i).^2).^2; 
   plot3(pp(1,i),pp(2,i),fp,'ro','MarkerSize',8,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);
end

