function refresh_peaks(x,y,xp,yp)

clf;

z = 3 * (1-x).^2 .* exp(-(x.^2) - (y+1).^2) - 10 * (x/5 - x.^3 - y.^5) .* exp(-x.^2 - y.^2) - 1/3 * exp(-(x+1).^2 - y.^2);
surfc(x,y,z,'EdgeColor',[.5 .5 .5]);
xlabel('x');
ylabel('y');
hold on;
zp = 3 * (1-xp).^2 .* exp(-(xp.^2) - (yp+1).^2) - 10 * (xp/5 - xp.^3 - yp.^5) .* exp(-xp.^2 - yp.^2) - 1/3 * exp(-(xp+1).^2 - yp.^2);
plot3(xp,yp,zp,'ro','MarkerSize',8,'MarkerFaceColor','r','MarkerEdgeColor','k','LineWidth',2);

return
