%
% This script is adapted from that which I used to generate figures for the
% Optimisation talk I gave in February 2022. The idea is to read the
% comments for a bit of a reminder of the theory, and then test the code.
% I tend to run 'matlab -nodesktop' and then cut and paste from an editor
% into the matlab terminal window. But I hope it works well in the matlab
% GUI too for those who prefer that.
% The practical will only cover Newton's Method (with its variant Levenberg)
% and the Gauss-Newton method (and its variant Levenberg-Marquardt). But 
% I think that will be plenty.
%
% Jesper Andersson
%

%
% Let us first take a little look at one of the functions that we will 
% use to test our minimisation algorithms. The Rosenbrock function, also
% known as the "Banana function" 
% (https://en.wikipedia.org/wiki/Rosenbrock_function). It is commonly
% used as a test case for non-linear optimisation methods, and in 
% particular methods that use the gradient of the cost-function in a
% naive way, like for example steepest-descent, tend to fare poorly.
% It is a 2D function, i.e. R^2->R.
%

[x,y] = ndgrid(-1.5:0.05:2,-1:0.05:3); % Grid on which to calculate function
rosen = (1-x).^2 + 100*(y-x.^2).^2;    % Function values on grid
figure('Position',[100 100 800 800]);
colormap(jet(64));
surfc(x,y,rosen,'EdgeColor',[.5 .5 .5]);
xlabel('x');
ylabel('y');


% From that plot you get an overview of the function, but it is very hard 
% to see along the valley where the actual minimum is located. So to aid in that
% we will make our own colormap that is densely sampled in the range of
% values that constitute the valley, and sparsely sampled elsewhere.

cm = colormap;
tmp = 1:0.001:64;
scm = zeros(length(tmp),3);
scm(:,1) = interp1(1:64,cm(:,1),tmp);
scm(:,2) = interp1(1:64,cm(:,2),tmp);
scm(:,3) = interp1(1:64,cm(:,3),tmp);
tmp1 = linspace(0.0001,max(rosen(:)),length(scm));
tmp2 = linspace(log(0.0001),log(max(rosen(:))),length(scm));
lcm(1,:) = scm(1,:);
for i=2:length(scm)-1
   indx = find(tmp2>log(tmp1(i)));
   wgt = (log(tmp1(i))-tmp2(indx(1)-1))/(tmp2(indx(1))-tmp2(indx(1)-1));
   lcm(i,1) = scm(indx(1)-1,1) + wgt*(scm(indx(1),1) - scm(indx(1)-1,1));
   lcm(i,2) = scm(indx(1)-1,2) + wgt*(scm(indx(1),2) - scm(indx(1)-1,2));
   lcm(i,3) = scm(indx(1)-1,3) + wgt*(scm(indx(1),3) - scm(indx(1)-1,3));
end
lcm(length(scm),:) = scm(length(scm),:);

% So now we can set this new colormap and look at the function again.

colormap(lcm);
axis([-1.5 2 -1 3 -200 2500]);
view(-28,30);

% That should give you an idea of where abouts the minimum is. But
% to be even clearer we can put a little marker at the (known)
% minimum at [1,1].

hold on;
xm=1; ym=1; fm=(1-xm).^2 + 100*(ym-xm.^2).^2;
plot3(xm,ym,fm,'ro','MarkerSize',12,'MarkerFaceColor','r','MarkerEdgeColor','k','LineWidth',3);

% The first thing we will do now is to look at the behaviour of 
% Newton's method (the ancestor of all optimisation methods). 
% As you hopefully recall we will first of all need a 
% starting point, from which we subsequently take a series
% of steps towards the minimum. Below I have suggested such
% a starting point, but feel free to play around with changing 
% it. 

% xp=1.6; yp=0; fp=(1-xp).^2 + 100*(yp-xp.^2).^2; % Starting point
xp=0; yp=1; fp=(1-xp).^2 + 100*(yp-xp.^2).^2; % Starting point
plot3(xp,yp,fp,'ro','MarkerSize',8,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

% Recall that what we do with Newton's method is that we replace the
% actual function (Rosenbrock) with a (series of) proxy-function
% that is linear in the parameters of interest. The latter is 
% what allows us to directly solve for the minimum. The 
% proxy function that we use is a second order Taylor expansion 
% of the original function. So hence we need to be able to 
% calculate the (vector of) first derivative and the (matrix of)
% second derivative of the function. And for this function it is
% trivial.

% f = (1-x).^2 + 100*(y-x.^2).^2
% df/dx = -2*(1-x) -400*x*(y-x^2)
% df/dy = 200*(y-x^2)
% d2f/dx2 = 2 + 1200*x^2 - 400*y
% d2f/dy2 = 200
% d2f/dxdy = -400*x

% We will now use these expressions to make our proxy-function
% centered on our starting point [xp,yp] (i.e. the expansion
% is around that point).

grad = [-2*(1-xp)-400*xp*(yp-xp^2) 200*(yp-xp^2)]'; % [df/dx; df/dy]
H = [2+1200*xp^2-400*yp -400*xp; -400*xp 200];             % [d2f/dx2 d2f/dxdy; d2f/dxdy d2f/dy2]
f_xp = (1-xp).^2 + 100*(yp-xp.^2).^2;               % Function value at starting/expansion point
p = [xp; yp];

% Our proxy-function is now:
% proxy(x,y) = f_xp + grad'*[x-xp; y-yp] + (1/2)*[x-xp; y-yp]'*H*[x-xp; y-yp];

% Let us now calculate this proxy-function for a neighbourhood 
% around our starting point. Note that for better visualisation 
% you might need to modify the neighbourhood if you have picked 
% a different starting point.

[xn,yn] = ndgrid(xp-.8:0.05:xp+.3,yp-1:0.05:yp+3);
proxy = f_xp + grad(1).*(xn-xp) + grad(2).*(yn-yp) + 0.5*H(1,1).*(xn-xp).^2 + 0.5*H(2,2).*(yn-yp).^2 + H(1,2).*((xn-xp).*(yn-yp));
surf(xn,yn,proxy);

% It might help to rotate the plot a little (using the button on
% the figure window) in order to get a better feel for what the 
% proxy function looks like, and for how it compares to the 
% original function.

% Now let us take a "Newton step" and look at where it falls
% on our proxy function.

p2 = [xp; yp] - inv(H)*grad;
next_proxy_f = f_xp + grad'*(p2-p) + (1/2)*(p2-p)'*H*(p2-p);
plot3([xp p2(1)]',[yp p2(2)]',[f_xp next_proxy_f],'bo-','MarkerSize',8,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

% Again it can be useful to rotate a little to get a better 
% feel for it. As you can see we have taken a step straight
% to the bottom of the "valley". Though we still have some
% way to "walk" along the valley to find the true minimum.

% Let us now take a couple of steps more. But in order to do
% that as easily as possible I have written a small script that
% "refreshes" the Rosenbrock plot. You can take a look at it if
% you want, but it isn't particularly interesting.

pp = [p p2];
refresh_rosenbrock(x,y,lcm,pp);

% And now we re-calculate the gradient and the Hessian for this
% new point. 

grad = [-2*(1-pp(1,end))-400*pp(1,end)*(pp(2,end)-pp(1,end)^2) 200*(pp(2,end)-pp(1,end)^2)]'; % [df/dx; df/dy]
H = [2+1200*pp(1,end)^2-400*pp(2,end) -400*pp(1,end); -400*pp(1,end) 200];             % [d2f/dx2 d2f/dxdy; d2f/dxdy d2f/dy2]
f_p = (1-pp(1,end)).^2 + 100*(pp(2,end)-pp(1,end).^2).^2;               % Function value at starting/expansion point

% And let's calculate the next proxy function in the 
% neighbourhood of the latest point. Again, you might need to
% adjust the neighbourhood for better visualisation if you 
% have picked a different starting point.

[xn,yn] = ndgrid(pp(1,end)-1:0.05:pp(1,end)+.5,pp(2,end)-3:0.05:pp(2,end)+0.5);
proxy = f_p + grad(1).*(xn-pp(1,end)) + grad(2).*(yn-pp(2,end)) + 0.5*H(1,1).*(xn-pp(1,end)).^2 + 0.5*H(2,2).*(yn-pp(2,end)).^2 + H(1,2).*((xn-pp(1,end)).*(yn-pp(2,end)));
surf(xn,yn,proxy);

% Again it might be worth rotating a bit.

% And then we take the next step, and display it.

next_p = pp(:,end) - inv(H)*grad;
pp = [pp next_p];
next_proxy_f = f_p + grad'*(pp(:,end)-pp(:,end-1)) + (1/2)*(pp(:,end)-pp(:,end-1))'*H*(pp(:,end)-pp(:,end-1));
plot3([pp(1,end-1) pp(1,end)]',[pp(2,end-1) pp(2,end)]',[f_p next_proxy_f],'bo-','MarkerSize',8,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

% Again it might be worth rotating a bit.

% And we show that new point on the original function

refresh_rosenbrock(x,y,lcm,pp);

% Hopefully you can see that it is already quite close to the
% true (known) minimum. Feel free to do a little bit of cutting
% and pasting to convince yourself that it would be _very_ close
% in another few iterations.


% Next, let us look at a different function to illustrate
% what might happen if you start out from a non-convex part
% of the cost-function landscape.
% So for this we will use the "peaks" function, which will
% surely be familiar to most Matlab users.

[x,y] = ndgrid(-3:0.1:3,-3:0.1:3);
z = 3 * (1-x).^2 .* exp(-(x.^2) - (y+1).^2) - 10 * (x/5 - x.^3 - y.^5) .* exp(-x.^2 - y.^2) - 1/3 * exp(-(x+1).^2 - y.^2);

figure('Position',[100 100 800 800]);
colormap(jet(64));
surfc(x,y,z,'EdgeColor',[.5 .5 .5]);
xlabel('x');
ylabel('y');

% And let's pick a starting point that _looks_ easy enough
% (but which might not be).

xp=1.1; yp=-0.8;
zp = 3 * (1-xp).^2 .* exp(-(xp.^2) - (yp+1).^2) - 10 * (xp/5 - xp.^3 - yp.^5) .* exp(-xp.^2 - yp.^2) - 1/3 * exp(-(xp+1).^2 - yp.^2);
hold on;
plot3(xp,yp,zp,'ro','MarkerSize',8,'MarkerFaceColor','r','MarkerEdgeColor','k','LineWidth',2);

% Next we need to calculate the gradient and the Hessian. 
% The expression for zp above might not look like much,
% but the expressions for the gradient and (in particular)
% the Hessian gets surprisingly messy. A little trick in 
% these situations is to use Matlab's symbolic toolbox.
% My suggestion, if you want to try, is to open a separate
% matlab window and type

syms xp yp zp
zp = 3 * (1-xp)^2 * exp(-(xp^2) - (yp+1)^2) - 10 * (xp/5 - xp^3 - yp^5) * exp(-xp^2 - yp^2) - 1/3 * exp(-(xp+1)^2 - yp^2);
df_dx = diff(zp,xp)
df_dy = diff(zp,yp)
d2f_dx2 = diff(zp,xp,2)
d2f_dy2 = diff(zp,yp,2)
d2f_dxdy = diff(zp,xp,yp)

% You can then use the resulting symbolic derivatives to
% cut and paste into "proper" Matlab code that can do the
% calculations.
%
% And this is what the gradient and Hessian looks like.
% I am glad I didn't have to do that by hand.
% It demonstrates a little why Newton's method isn't
% everyone's cup of tea.
%
xp=1.1; yp=-0.8;

zp = 3 * (1-xp).^2 .* exp(-(xp.^2) - (yp+1).^2) - 10 * (xp/5 - xp.^3 - yp.^5) .* exp(-xp.^2 - yp.^2) - 1/3 * exp(-(xp+1).^2 - yp.^2);

grad = [[(exp(- (xp + 1)^2 - yp^2)*(2*xp + 2))/3 + 3*exp(- (yp + 1)^2 - xp^2)*(2*xp - 2) + exp(- xp^2 - yp^2)*(30*xp^2 - 2) - 6*xp*exp(- (yp + 1)^2 - xp^2)*(xp - 1)^2 - 2*xp*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5)] [(2*yp*exp(- (xp + 1)^2 - yp^2))/3 + 50*yp^4*exp(- xp^2 - yp^2) - 3*exp(- (yp + 1)^2 - xp^2)*(2*yp + 2)*(xp - 1)^2 - 2*yp*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5)]]';

H = [[(2*exp(- (xp + 1)^2 - yp^2))/3 + 6*exp(- (yp + 1)^2 - xp^2) + 60*xp*exp(- xp^2 - yp^2) - 6*exp(- (yp + 1)^2 - xp^2)*(xp - 1)^2 - 2*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5) - (exp(- (xp + 1)^2 - yp^2)*(2*xp + 2)^2)/3 + 12*xp^2*exp(- (yp + 1)^2 - xp^2)*(xp - 1)^2 + 4*xp^2*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5) - 12*xp*exp(- (yp + 1)^2 - xp^2)*(2*xp - 2) - 4*xp*exp(- xp^2 - yp^2)*(30*xp^2 - 2)] [4*xp*yp*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5) - (2*yp*exp(- (xp + 1)^2 - yp^2)*(2*xp + 2))/3 - 2*yp*exp(- xp^2 - yp^2)*(30*xp^2 - 2) - 100*xp*yp^4*exp(- xp^2 - yp^2) - 3*exp(- (yp + 1)^2 - xp^2)*(2*xp - 2)*(2*yp + 2) + 6*xp*exp(- (yp + 1)^2 - xp^2)*(2*yp + 2)*(xp - 1)^2]; [4*xp*yp*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5) - (2*yp*exp(- (xp + 1)^2 - yp^2)*(2*xp + 2))/3 - 2*yp*exp(- xp^2 - yp^2)*(30*xp^2 - 2) - 100*xp*yp^4*exp(- xp^2 - yp^2) - 3*exp(- (yp + 1)^2 - xp^2)*(2*xp - 2)*(2*yp + 2) + 6*xp*exp(- (yp + 1)^2 - xp^2)*(2*yp + 2)*(xp - 1)^2] [(2*exp(- (xp + 1)^2 - yp^2))/3 - 6*exp(- (yp + 1)^2 - xp^2)*(xp - 1)^2 - 2*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5) + 200*yp^3*exp(- xp^2 - yp^2) - 200*yp^5*exp(- xp^2 - yp^2) - (4*yp^2*exp(- (xp + 1)^2 - yp^2))/3 + 4*yp^2*exp(- xp^2 - yp^2)*(10*xp^3 - 2*xp + 10*yp^5) + 3*exp(- (yp + 1)^2 - xp^2)*(2*yp + 2)^2*(xp - 1)^2]]

% Exactly as before the proxy function is given by
%
% proxy(x,y) = zp + grad'*[x-xp; y-yp] + (1/2)*[x-xp; y-yp]'*H*[x-xp; y-yp];
%
% So let us now calculate it in a neighbourhood around
% our starting guess [xp yp].

[xn,yn] = ndgrid(xp-.8:0.05:xp+1,yp-.2:0.05:yp+1.5);
proxy = zp + grad(1).*(xn-xp) + grad(2).*(yn-yp) + 0.5*H(1,1).*(xn-xp).^2 + 0.5*H(2,2).*(yn-yp).^2 + H(1,2).*((xn-xp).*(yn-yp));
alpha(0.5); % To improve visibility in the 3D plot
surf(xn,yn,proxy);

% Again, a little rotating might help with the visualisation
%
% What you should have gleaned from this is that our proxy
% function is indeed a very good local fit to the actual 
% function. But also that it has "fitted the peak" rather
% than the "bowl".
%
% And if we take a Newton step we should be able to convince
% ourselves that we are going in the wrong direction.
%

next_p = [xp; yp] - inv(H)*grad;
pp = [[xp; yp] next_p];
proxy_f = zp + grad'*(pp(:,2)-pp(:,1)) + (1/2)*(pp(:,2)-pp(:,1))'*H*(pp(:,2)-pp(:,1));
plot3(pp(1,2),pp(2,2),proxy_f,'ro','MarkerSize',8,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

% A little rotating should convince you that we have (correctly)
% taken a step to an extreme point of our proxy function. But as
% we know, that wasn't really what we wanted.
% Ultimately the problem is that our starting point was in a
% "concave" area, which means that the quadratic in our Taylor-
% expansion has a negative sign (negative definite matrix, since 
% it is a multi-variate expansion).
% The "solution" is to perturb the Hessian until it becomes 
% positive definite (or at least until it yields a step that
% means that our cost-function (N.B. the true function, not the
% proxy) decreases. The perturbation is done by adding some
% "fudge factor" to the diagonal elements of the Hessian.
%
% I suggest you take a look at H and decide on a value for 
% the fudge factor (lambda) and test if that has "solved"
% the problem.
%

lambda = 15; % Your value goes here
pH = H + lambda * eye(2);
proxy = zp + grad(1).*(xn-xp) + grad(2).*(yn-yp) + 0.5*pH(1,1).*(xn-xp).^2 + 0.5*pH(2,2).*(yn-yp).^2 + pH(1,2).*((xn-xp).*(yn-yp));
% Show what the resulting proxy function looks like
refresh_peaks(x,y,xp,yp); % Similar to refresh_rosenbrock
alpha(0.5); % To improve visibility in the 3D plot
surf(xn,yn,proxy);
pause(5); % To allow some time to see the proxy function
% Next show where the corresponding step takes us
np = [xp; yp] - inv(pH)*grad;
pp = [[xp; yp] np];
real_f = 3 * (1-np(1)).^2 .* exp(-(np(1).^2) - (np(2)+1).^2) - 10 * (np(1)/5 - np(1).^3 - np(2).^5) .* exp(-np(1).^2 - np(2).^2) - 1/3 * exp(-(np(1)+1).^2 - np(2).^2);
refresh_peaks(x,y,xp,yp);
plot3(pp(1,2),pp(2,2),real_f,'ro','MarkerSize',10,'MarkerFaceColor','b','MarkerEdgeColor','y','LineWidth',3);

% Again, take some time to rotate it to see where the new point ended up.
% Note that the new point is blue with yellow border. This is to allow it
% to be easily seen regardless of where on the surface it ends up.
% If you happened to pick lambda=15, you would have seen it take a step
% almost straight down to the minimum. That is a _fluke_. What you expect
% from a step with a non-negligible lambda is for it to take a step in
% "the right direction", i.e. so that the cost-function decreases. You
% don't necessarily expect the step to be very good.

% As before, you can keep playing with the lines of code above. Cutting
% and pasting, and following the algorithm as it proceeds towards the 
% minimum.

% This modification of Newton's method is called the "Levenberg method".
% The full algorithm proceeds as
%
% lambda = 0.01;  % Some small value
% p = [xp yp ...];    % Some starting point
% while (~converged)
%    calculate f, grad and H at p
%    pH = H + lambda * eye();
%    np = p - inv(pH)*grad;
%    calculate f at np
%    if (f(np) < f(p)) % If step successful
%       p = np;
%       lambda *= 0.1;
%    else % If step unsuccessful
%       lambda *= 10;
%    end
% end
%

%
% The final thing we will do in this practical is to take a look at
% a variant of Newton's method called "The Gauss-Newton method".
% It seems kind of likely that, between them, Gauss and Newton should
% be able to come up with something quite good.
% 
% The Gauss-Newton method appplies when the cost-function is a 
% sum-of-squared differences between some data and a model 
% prediction. In that situation we can replace the "true" Hessian
% with an approximation. If we assume normal distributed that
% approximation is identical to the expected value of the Hessian,
% which means that it is a special case of Fisher's scoring.
%
% First of all we generate some data with y = b1*exp(b2*x) + e
% where e ~N(0,sigma2). 
% b = [5 0.15];
% x = 10*rand(20,1);  % 20 random values between 0 and 10
% y = b(1)*exp(b(2)*x) + 0.5*randn(20,1); % "data"
%
% But instead of letting you generate it yourselves, I suggest
% you use some data that I already generated with the code above.
%

load data_for_fitting
figure;
plot(x,y,'*');

% And if we compare it to the "known model"

hold on;
xx = linspace(0,10);
yy = b(1)*exp(b(2)*xx);
plot(xx,yy,'r');

% But let's pretend we don't know the true parameters, and
% see if we are able to find them. 
%
% Our cost function is sum-of-squared differences between
% data and model predictions. This can be written as:

cf = (y - b(1)*exp(b(2)*x))'*(y - b(1)*exp(b(2)*x))

% which is the same as

cf = sum((y - b(1)*exp(b(2)*x)).^2)

% The former formulation is more useful for our calculations,
% but the latter might be easier to think about when we derive
% expressions for the derivatives and the Hessian.
% Let us denote our generative model by f(b1,b2:x), i.e. it is a 
% function of b1 and b2, given x. And remember that once we have the
% data x is a constant.
% That lets us write
%
% cf = sum((y-f(b1,b2:x))^2)
% So, just using the chain rule we see that
% dcf/db1 = sum(-2 * df/db1 * (y-f(b1,b2:x)))
% dcf/db2 = sum(-2 * df/db2 * (y - b(1)*exp(b(2)*x)));
% 
% or in Matlab

dcf_db1 = sum(-2*exp(b(2)*x).*(y - b(1)*exp(b(2)*x)));
dcf_db2 = sum(-2*b(1)*x.*exp(b(2)*x).*(y - b(1)*exp(b(2)*x)));
grad = [dcf_db1 dcf_db2]';

%
% And for the second derivatives we use both the product and
% the chain rule.
%
% d^2cf/db1^2 = sum(-2*d^2f/db1^2*(y-f(b1,b2:x)) + 2*SQR(df/db1))
% d^2cf/db2^2 = sum(-2*d^2f/db2^2*(y-f(b1,b2:x)) + 2*SQR(df/db2))
% d^2cf/db1db2 = sum(-2*d^2f/db1db2*(y-f(b1,b2:x)) + 2*df/db1*dfdb2)
%
% Where I used SQR to distinguish it from ^2, which I used for 
% 2nd derivative.
%
% Or in Matlab

d2cf_db12 = sum(2*exp(b(2)*x).^2);
d2cf_db22 = sum(-2*b(1)*x.^2.*exp(b(2)*x).*(y-b(1)*exp(b(2)*x)) + 2*b(1)^2*x.^2.*exp(2*b(2)*x));
d2cf_db1db2 = sum(-2*x.*exp(b(2)*x).*(y-b(1)*exp(b(2)*x)) + 2*b(1)*x.*exp(2*b(2)*x));

%
% So, let us now take a look at our cost-function landscape
%

[beta1,beta2] = ndgrid(3:0.1:6,-0.05:0.005:0.2);
sse = zeros(size(beta1));
for i=1:size(beta1,1)
   for j=1:size(beta1,2)
      sse(i,j) = (y - beta1(i,j)*exp(beta2(i,j)*x))' * (y - beta1(i,j)*exp(beta2(i,j)*x));
   end
end
figure('Position',[100 100 800 800]);
colormap(jet(64));
surfc(beta1,beta2,sse,'EdgeColor',[.5 .5 .5]);
xlabel('b1');
ylabel('b2');
view(-70,52);
axis([3 6 -0.05 0.2 0 4000]);

% And here we do the same thing as before with the colormap to make it 
% more "informative" in the area around the minimum.

cm = colormap;
tmp = 1:0.001:64;
scm = zeros(length(tmp),3);
scm(:,1) = interp1(1:64,cm(:,1),tmp);
scm(:,2) = interp1(1:64,cm(:,2),tmp);
scm(:,3) = interp1(1:64,cm(:,3),tmp);
tmp1 = linspace(0.0001,max(sse(:)),length(scm));
tmp2 = linspace(log(0.0001),log(max(sse(:))),length(scm));
lcm(1,:) = scm(1,:);
for i=2:length(scm)-1
   indx = find(tmp2>log(tmp1(i)));
   wgt = (log(tmp1(i))-tmp2(indx(1)-1))/(tmp2(indx(1))-tmp2(indx(1)-1));
   lcm(i,1) = scm(indx(1)-1,1) + wgt*(scm(indx(1),1) - scm(indx(1)-1,1));
   lcm(i,2) = scm(indx(1)-1,2) + wgt*(scm(indx(1),2) - scm(indx(1)-1,2));
   lcm(i,3) = scm(indx(1)-1,3) + wgt*(scm(indx(1),3) - scm(indx(1)-1,3));
end
lcm(length(scm),:) = scm(length(scm),:);
colormap(lcm);

% And let us pick a starting point and show it on the graph
% Feel free to pick a different one to me.
b = [4 0.08];
fb = (y - b(1)*exp(b(2)*x))' * (y - b(1)*exp(b(2)*x));
hold on;
plot3(b(1),b(2),fb,'ro','MarkerSize',12,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

% Looks like it should be an easy starting point, no?

% First let us use the full Newton Hessian (as per the 
% expressions above).

dcf_db1 = sum(-2*exp(b(2)*x).*(y - b(1)*exp(b(2)*x)));
dcf_db2 = sum(-2*b(1)*x.*exp(b(2)*x).*(y - b(1)*exp(b(2)*x)));
grad = [dcf_db1 dcf_db2]';

d2cf_db12 = sum(2*exp(b(2)*x).^2);
d2cf_db22 = sum(-2*b(1)*x.^2.*exp(b(2)*x).*(y-b(1)*exp(b(2)*x)) + 2*b(1)^2*x.^2.*exp(2*b(2)*x));
d2cf_db1db2 = sum(-2*x.*exp(b(2)*x).*(y-b(1)*exp(b(2)*x)) + 2*b(1)*x.*exp(2*b(2)*x));
H = [d2cf_db12 d2cf_db1db2; d2cf_db1db2 d2cf_db22];

% And see what proxy function that gives us

[b1n b2n] = ndgrid(3:0.1:5,-0.05:0.005:0.12);
proxy = fb + grad(1).*(b1n-b(1)) + grad(2).*(b2n-b(2)) + 0.5*H(1,1).*(b1n-b(1)).^2 + 0.5*H(2,2).*(b2n-b(2)).^2 + H(1,2).*((b1n-b(1)).*(b2n-b(2)));
hold on;
alpha(0.5)
surf(b1n,b2n,proxy);
 
% We can see that it "curves off" in the wrong direction,
% indicating that there is a maximum somewhere way off
% in the "negative b1 direction".

% So, what about the Gauss-Newton Hessian? Let us first look
% at the expressions for the Newton Hessian again.
%
% d^2cf/db1^2 = sum(-2*d^2f/db1^2*(y-f(b1,b2:x)) + 2*SQR(df/db1))
% d^2cf/db2^2 = sum(-2*d^2f/db2^2*(y-f(b1,b2:x)) + 2*SQR(df/db2))
% d^2cf/db1db2 = sum(-2*d^2f/db1db2*(y-f(b1,b2:x)) + 2*df/db1*dfdb2)
%
% We can see that each term is a sum of two terms, one of which
% is some "constant" multiplied by (y - f(b1,b2:x)). The reasoning
% behind that Gauss-Newton approximation is that, summed across 
% all data points y, that term ought to be ~0. So if we just do
% away with that the Hessian simplifies to
%
% d^2cf/db1^2 = sum(2*SQR(df/db1))
% d^2cf/db2^2 = sum(2*SQR(df/db2))
% d^2cf/db1db2 = sum(2*df/db1*dfdb2)
%
% which looks a lot simpler. And importantly we also see that
% the elements on the diagonal are sums-of-squares, and hence 
% guaranteed to be positive. In fact we will soon see that the
% whole matrix is of the form J'*J, where J is a matrix, so is
% guaranteed to be positive definite.
%
% But for now, let us calculate and show the proxy function
% given by the Gauss-Newton Hessian.

d2cf_db12 = sum(2*exp(b(2)*x).^2);
d2cf_db22 = sum(2*b(1)^2*x.^2.*exp(2*b(2)*x));
d2cf_db1db2 = sum(2*b(1)*x.*exp(2*b(2)*x));
H = [d2cf_db12 d2cf_db1db2; d2cf_db1db2 d2cf_db22];

[b1n b2n] = ndgrid(3:0.1:5,-0.05:0.005:0.12);
proxy = fb + grad(1).*(b1n-b(1)) + grad(2).*(b2n-b(2)) + 0.5*H(1,1).*(b1n-b(1)).^2 + 0.5*H(2,2).*(b2n-b(2)).^2 + H(1,2).*((b1n-b(1)).*(b2n-b(2)));
refresh_sse(beta1,beta2,sse,lcm,[b fb]);
hold on;
alpha(0.5)
surf(b1n,b2n,proxy);

% And we can see that our proxy function now seems to "bend off"
% in the right direction, indicating a minimum "sort of in the
% right ball park". But maybe we should check.

nb = b' - inv(H)*grad;
fnb = (y - nb(1)*exp(nb(2)*x))' * (y - nb(1)*exp(nb(2)*x));
plot3(nb(1),nb(2),fnb,'ro','MarkerSize',12,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

% Interestingly we see that is has actually "overshot" the 
% target a little. It has gone in the right direction, i.e.
% towards the minimum, but a little bit too far.
% This might be the time to mention that there is also a 
% version of Levenberg's algorithm for Gauss-Newton, and
% usually in this context it is called "Levenberg-Marquardt".
% Instead of adding a fixed value to the diagonal (as in
% Levenberg), it adds a multiple of the diagonal itself to
% the diagonal. We can do that because we already known that
% all the values on the diagonal are positive. Let's have
% a little go.

lambda = 1; % Feel free to pick a different value
pH = H + lambda*diag(diag(H));

proxy = fb + grad(1).*(b1n-b(1)) + grad(2).*(b2n-b(2)) + 0.5*pH(1,1).*(b1n-b(1)).^2 + 0.5*pH(2,2).*(b2n-b(2)).^2 + pH(1,2).*((b1n-b(1)).*(b2n-b(2)));
refresh_sse(beta1,beta2,sse,lcm,[b fb]);
hold on;
alpha(0.5)
surf(b1n,b2n,proxy);

nb = b' - inv(pH)*grad;
fnb = (y - nb(1)*exp(nb(2)*x))' * (y - nb(1)*exp(nb(2)*x));
plot3(nb(1),nb(2),fnb,'ro','MarkerSize',12,'MarkerFaceColor','b','MarkerEdgeColor','k','LineWidth',2);

% And the full Levenberg-Marquardt algorithm proceeds analogously
% to the Levenberg (see pseudo code above). Feel free to take a
% few more steps, or even implement your own Levenberg-Marquardt
% algorithm.

% The last thing we are going to do is to introduce you to the 
% concept of the Jacobian. The Jacobian is simply the Nxm matrix
% that contains the partial derivatives of the generative model
% (our f above) for all the points x of the independent variable.
% The first column has the partial derivatives w.r.t. the first
% parameter (b1 in our example) and the second column those w.r.t.
% the second parameter. The rows correspond to the different data
% points, i.e. the different values for our independent variable 
% x. So in our example with 20 data points and 2 parameters, the
% Jacobian is a 20x2 matrix.

J = [-2*exp(b(2)*x) -2*b(1)*x.*exp(b(2)*x)];

% Now, the nice thing about this formulation is that we can
% express the gradient and the Gauss-Newton Hessian as

grad = J'*(y-b(1)*exp(b(2)*x));
H = (1/2)*J'*J;

% You can compare for yourselves to convince yourselves 
% that it is correct.

% And that is all for today folks.











